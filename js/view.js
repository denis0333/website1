//insertar la tabla




import AddTodo from "./components/add-todo.js"
export default class View {
	constructor() {
		this.model = null;
		this.table = document.getElementById('table');
		this.AddTodoForm = new AddTodo()
		this.AddTodoForm.onclick((title, description) => this.addTodo(title, description))
	}

	setModel(model) {
		this.model = model;
	}

	//inicializa ponendo filas a la tabla si existen en el local storage
	render() {
		const todos = this.model.getTodos();
		for (const todo of todos){
			this.createRow(todo)
		}
	}

	//agrega la fila al HTML
	//manda el titulo y la descipcion al modelo 
	addTodo(title, description) {
		const todo = this.model.addTodo(title, description)
		this.createRow(todo)
	}

	//remueve del HTML y del local storage
	removeTodo(id){
		this.model.removeTodo(id);
		document.getElementById(id).remove();
	}

	//crea la tabla
	createRow(todo) {
		const row = table.insertRow()
		row.setAttribute('id', todo.id)
		row.innerHTML = `
		<td>${todo.title}</td>
		<td>${todo.description}</td>
		<td class= "text-center">
		</td>
		`

		const removeBtn = document.createElement('button');
		removeBtn.classList.add('btn', 'btn-danger')
		removeBtn.innerText = 'Remove';
		removeBtn.onclick = () => this.removeTodo(todo.id)
		row.children[2].appendChild(removeBtn)
	}
}