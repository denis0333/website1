//modelo de la tabla




export default class Model {
	constructor() {
		//inicializa el local storage
		this.view = null;
		this.todos = JSON.parse(localStorage.getItem('todos'))
		//si no tiene datos añade uno por defecto
		if (!this.todos || this.todos.length < 1){
			this.todos = [
				{
					'id' : 0,
					'title' : 'Learn JS',
					'description' : 'Easy'
				}
			]
			this.currentId = 1;
		}else{
			this.currentId = this.todos[this.todos.length - 1].id + 1;
		}
	}
	setView(view) {
		this.view = view;
	}

	//guarda los datos
	save(){
		localStorage.setItem('todos', JSON.stringify(this.todos))
	}

	getTodos() {
		return this.todos;
	}

	//almacena los datos en el local storage
	addTodo(title, description) {
		const todo = {
			'id' : this.currentId++,
			'title': title,
			'description':description
		}

		this.todos.push(todo);
		this.save()
		return {...todo}
	}

	////remueve datos del local storage
	removeTodo(id){
		const index = this.todos.findIndex((todo) => todo.id === id)
		this.todos.splice(index, 1)	
		this.save()
	}
}
